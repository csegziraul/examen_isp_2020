import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Win extends JFrame {
    private JTextField tFile1, tFile2, tFile3;
    private JButton but;
    public Win(){
        this.setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setVisible(true);
        setSize(120,240);
    }
    public void init(){
        tFile1 = new JTextField();
        tFile1.setBounds(20,20,80,20);

        tFile2 = new JTextField();
        tFile2.setBounds(20,60,80,20);

        tFile3 = new JTextField();
        tFile3.setBounds(20,100,80,20);

        but = new JButton("munceste");
        but.setBounds(20,140,80,40);

        but.addActionListener(new Actiune());

        add(tFile3); add(tFile1); add(tFile2); add(but);
    }
    class Actiune implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {

            if((tFile1.getText() != null) && (tFile2.getText() != null)){
                double x,y,z;
                x = Double.parseDouble(tFile1.getText());
                y = Double.parseDouble(tFile2.getText());
                z = x*y;
                tFile3.setText(String.valueOf(z));
            }
;        }
    }
}
class Main{
    public static void main(String[] args) {
        new Win();
    }
}
